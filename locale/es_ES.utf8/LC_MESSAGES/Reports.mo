��    #      4  /   L           	  9     :   L  9   �  =   �  
   �     
  :     =   M     �     �  	   �     �     �     �  	   �     �  	   �     �  	   �            .   6  2   e     �     �     �     �     �     �     �  
   �            �       �  G     F   N  G   �  J   �     (	     8	  F   G	  J   �	     �	     �	  	   �	     �	  	   �	     
  
   
  	   &
  	   0
     :
  
   U
     `
     {
  .   �
  >   �
          &     ;     B     R     e     v     �     �  
   �               	            "                                                           #   !                         
                                           %s years A field cannot be placed here; choose a function instead. A field cannot be placed here; choose an operator instead. A function cannot be placed here; choose a field instead. A function cannot be placed here; choose an operator instead. Add Column Add Row An operator cannot be placed here; choose a field instead. An operator cannot be placed here; choose a function instead. Back Between Breakdown Calculation Calculations Calculations Reports Constants Equation Functions Graph Results Operators Please choose a field. Please choose a function. Please choose an operator or another constant. Please choose an operator or press save to finish. Please choose an operator. Program Title Run Save Report Saved Equation Saved Report Saved Reports Student ID Time Values Untitled Project-Id-Version: Reports module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:28+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %s años Un campo no se puede colocar acá; escoja una función en lugar de eso. Un campo no se puede colocar acá; escoja un operador en lugar de eso. Una función no se puede colocar acá; escoja un campo en lugar de eso. Una función no se puede colocar acá; escoja un operador en lugar de eso. Agregar Columna Agregar Línea Un operador no se puede colocar acá; escoja un campo en lugar de eso. Un operador no se puede colocar acá; escoja una función en lugar de eso. Regresar Entre Análisis Cálculo Cálculos Reportes de Cálculos Constantes Ecuación Funciones Gráfico de los Resultados Operadores Por favor escoja un campo. Por favor escoja una función. Por favor escoja un operador o otra constante. Por favor escoja un operador o presione guardar para terminar. Por favor escoja un operador. Título del Programa Correr Guardar Reporte Ecuación Guardada Reporte Guardado Reportes Guardados ID Estudiante Valores de Tiempo Sans titre 