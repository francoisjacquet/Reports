��    #      4  /   L           	  9     :   L  9   �  =   �  
   �     
  :     =   M     �     �  	   �     �     �     �  	   �     �  	   �     �  	   �            .   6  2   e     �     �     �     �     �     �     �  
   �            �       �  ;   �  H     ;   P  L   �     �     �  :   �  >   5	     t	     {	     �	     �	  	   �	     �	  
   �	  	   �	  	   �	     �	  
   �	     �	      
  2   5
  A   h
     �
     �
     �
     �
     �
     	          -     9     J               	            "                                                           #   !                         
                                           %s years A field cannot be placed here; choose a function instead. A field cannot be placed here; choose an operator instead. A function cannot be placed here; choose a field instead. A function cannot be placed here; choose an operator instead. Add Column Add Row An operator cannot be placed here; choose a field instead. An operator cannot be placed here; choose a function instead. Back Between Breakdown Calculation Calculations Calculations Reports Constants Equation Functions Graph Results Operators Please choose a field. Please choose a function. Please choose an operator or another constant. Please choose an operator or press save to finish. Please choose an operator. Program Title Run Save Report Saved Equation Saved Report Saved Reports Student ID Time Values Untitled Project-Id-Version: Reports module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:29+0200
Last-Translator: Emerson Barros
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %s anos Um campo não pode ser colocado aqui; escolha uma função. Um campo não pode ser colocado aqui; em vez disso, escolha um operador. Uma função não pode ser colocada aqui; escolha um campo. Uma função não pode ser colocada aqui; em vez disso, escolha um operador. Adicionar coluna Adicionar linha Um operador não pode ser colocado aqui; escolha um campo. Um operador não pode ser colocado aqui; escolha uma função. Voltar Entre Análise Cálculo Cálculos Relatórios de cálculos Constantes Equação Funções Gráfico de resultados Operadores Por favor, escolha um campo. Por favor, escolha uma função. Por favor, escolha um operador ou outra constante. Por favor, escolha um operador ou pressione salvar para terminar. Por favor, escolha um operador. Título do programa Executar Salvar relatório Equação salva Relatório salvo Relatórios salvos ID do aluno Valores de tempo Sem título 