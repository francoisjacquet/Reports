��    "      ,  /   <      �     �  9     :   <  9   w  =   �  
   �     �  :     =   =     {     �     �     �     �  	   �     �  	   �     �  	   �     �       .     2   K     ~     �     �     �     �     �     �  
   �     �     �  �       �  A   �  C   ,  A   p  F   �     �     	  C   	  F   X	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	  
   �	     
     
  &   6
  9   ]
     �
     �
     �
     �
     �
     �
       	        !     4               	            !                                                                                         
                   "                       %s years A field cannot be placed here; choose a function instead. A field cannot be placed here; choose an operator instead. A function cannot be placed here; choose a field instead. A function cannot be placed here; choose an operator instead. Add Column Add Row An operator cannot be placed here; choose a field instead. An operator cannot be placed here; choose a function instead. Back Between Calculation Calculations Calculations Reports Constants Equation Functions Graph Results Operators Please choose a field. Please choose a function. Please choose an operator or another constant. Please choose an operator or press save to finish. Please choose an operator. Program Title Run Save Report Saved Equation Saved Report Saved Reports Student ID Time Values Untitled Project-Id-Version: Reports module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:29+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %s leta Tukaj ni mogoče postaviti polja; namesto tega izberite funkcijo. Tukaj ni mogoče postaviti polja; namesto tega izberite operaterja. Tukaj ni mogoče postaviti funkcije; namesto tega izberite polje. Tukaj ni mogoče postaviti funkcije; namesto tega izberite operaterja. Dodaj Kolono Dodaj Vrstico Tukaj ni mogoče postaviti operaterja; namesto tega izberite polje. Tukaj ni mogoče postaviti operaterja; namesto tega izberite funkcijo. Nazaj Med Kalkulacija Kalkulacije Poročila o izračunih Konstante Enačba Funkcije Graf Operaterji Prosimo izberite polje. Prosimo izberite funkcijo. Izberite operator ali drugo konstanto. Izberite operaterja ali pritisnite Shrani za dokončanje. Prosimo izberite operaterja. Naslov Programa Poženi Shrani Poročilo Shranjena enačba Shranjeno Poročilo Shranjena Poročila ID dijaka Časovne vrednosti Brez naslova 