/**
 * Calculations program JS
 *
 * @package Reports module
 */

$(document).ready(function() {
	$('.reports-calculation-item').on('click', function() {
		insertItem(
			$(this).data('title'),
			$(this).data('type')
		);

		return false;
	});

	$('.reports-calculation-backspace').on('click', backspace);

	$('.reports-calculation-runquery').on('click', runQuery);

	$('.reports-calculation-savescreen').on('click', function() {
		document.getElementById('save_screen').style.display = 'inline-block';

		return false;
	});

	$('.reports-calculation-title').on('keypress', function(event) {
		if (event.keyCode == 13) {
			saveQuery();

			return false;
		}
	});

	$('.reports-calculation-save').on('click', function() {
		saveQuery();

		return false;
	});

	// Fix function called as many times as we browsed the page in AJAX / loaded this JS file
	$(document).off('change', '.reports-calculation-switch-search-input');
	$(document).on('change', '.reports-calculation-switch-search-input', function() {
		switchSearchInput(this);
	});

	// Fix function called as many times as we browsed the page in AJAX / loaded this JS file
	$(document).off('click', '.reports-calculation-new-search-item');
	$(document).on('click', '.reports-calculation-new-search-item', newSearchItem);

	// Fix function called as many times as we browsed the page in AJAX / loaded this JS file
	$(document).off('click', '.reports-calculation-remove-search-item');
	$(document).on('click', '.reports-calculation-remove-search-item', function() {
		var id = $(this).parents('div').attr('id').replace('search_', '');

		removeSearchItem(id);
	});
});
