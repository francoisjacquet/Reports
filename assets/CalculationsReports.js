/**
 * Calculations Reports program JS
 *
 * @package Reports module
 */

var cols = 1,
	rows = 1;

function addCol()
{
	cols++;

	for ( row = 1; row <= rows; row++ )
	{
		addCell( cols, row );
	}

	button_left = parseInt(document.getElementById('add_col').style.left);

	document.getElementById('add_col').style.left = (button_left + 220) + 'px';
}

function addRow()
{
	rows++;

	for ( col = 1; col <= cols; col++ )
	{
		addCell( col, rows );
	}

	button_top = parseInt(document.getElementById('add_row').style.top);

	document.getElementById('add_row').style.top = (button_top + 120) + 'px';
}

function addCell( col, row )
{
	width = 220;
	height = 120;

	x = 10 + width * (col-1);
	y = 10 + height * (row-1);

	document.getElementById('main_div').innerHTML = document.getElementById('main_div').innerHTML +
		replaceAll(
			document.getElementById('new_cell').innerHTML
				.replace('9876',y)
				.replace('6789',x)
				.replace('div_id','id'),
			'cell_id',
			'['+row+']['+col+']'
		);
}

function replaceAll( haystack, needle, replacement )
{
	haystack = haystack.replace(needle,replacement);

	if( haystack.match( needle ) )
		haystack = replaceAll(haystack,needle,replacement);

	return haystack;
}

addCell(1,1);

// Fix function called as many times as we browsed the page in AJAX / loaded this JS file
$(document).off('click', '.onclick-add-col');
$(document).on('click', '.onclick-add-col', addCol);

// Fix function called as many times as we browsed the page in AJAX / loaded this JS file
$(document).off('click', '.onclick-add-row');
$(document).on('click', '.onclick-add-row', addRow);
