<?php
/**
 * Module Functions
 * (Loaded on each page)
 *
 * @package Reports module
 */


/**
 * Reports module Bottom Buttons.
 * Messaging new messages note.
 *
 * @uses Bottom.php|bottom_buttons hook
 *
 * @return true if bottom button, else false.
 */
function ReportsBottomButtons( $tag )
{
	if ( ( ! empty( $_SESSION['ReportsBottomButtons'] ) && $tag === 'ProgramFunctions/Bottom.fnc.php|bottom_buttons' )
		|| ! User( 'PROFILE' ) === 'admin'
		|| ! AllowEdit( 'Reports/SavedReports.php' )
		|| empty( $_SESSION['List_PHP_SELF'] ) )
	{
		return false;
	}

	?>
	<script src="modules/Reports/assets/ReportsBottomButtons.js?v=11.1"></script>
	<a href="Modules.php?modname=Reports/SavedReports.php&amp;modfunc=new&request_vars=" class="BottomButton"
		id="BottomButtonSaveReport">
		<img src="assets/themes/<?php echo Preferences( 'THEME' ); ?>/btn/download.png" />
		<span><?php echo dgettext( 'Reports', 'Save Report' ); ?></span>
	</a>
	<?php

	// Only add button once.
	$_SESSION['ReportsBottomButtons'] = true;

	return true;
}

add_action( 'Bottom.php|bottom_buttons', 'ReportsBottomButtons', 1 );

// @since RosarioSIS 12.0
add_action( 'ProgramFunctions/Bottom.fnc.php|bottom_buttons', 'ReportsBottomButtons', 1 );
